portion de la majeure partie des imp�ts est restee � la Charge de la commune.
Nous profitons donc de l'occasion que nous procure le pere le plus tendre pour nous jeter � ses pieds et le prions instam-ment de mettre fm � toutes nos calamites, secondes dans nos vues par la partie respectable du premicr et second ordre du Clerge qui, par leurs enseignements, nous ont fait supporter avec patience notre triste sort, par le zele patriotique de notre ancienne noblesse ;
Que leurs Privileges honorifiques leur soient conserves ;
Que la venalite des eharges soit supprimee et remboursee, remplies par le merite ;
Que toutes les fmances soient supprimees, comme les aides, les gabelies, les traites, les droits du domaine sur les eontr�les, sur les cuirs, toutes les impositions de taille, capitation acces-soire, vingtiemes, corvees et autres, sous quelque denomma-tion que ce soit, soient eteintes et annullees ;
Que toutes les rentes et dettes de l'Etat soient assurees et payees ; .	...
Qu'� cet eilet, il soit fait une imposition sur tous les biens existants et une sur chaque individu indistinctement de rang et de richesse.
Pour ce, les deputes aux Etats generaux seront autorises � demander quels sont les droits de la Nation, � les conso-lider avant tout, � voter par tete et non par ordre, ce qui met-trait le Tiers etat toujours dans l'asservissement des deux autres ordres, et � accorder telles impositions sur les trois ordres, egalement reparties d'apres leur richesse, qu'il plaira au Roi et aux Etats generaux d'arreter.
Tous les trois ordres ont un interet commun � procurer le retablissement des fmances ; que la concorde et l'amitie regnent entre eux ; de l� depend la felicite publique, le bon-heur de la patrie et la gloire du tr�ne.
