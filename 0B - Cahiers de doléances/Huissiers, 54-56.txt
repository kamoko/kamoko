HUISSIERS
Cahier de doldances, plaintes et remontrances des huissiers royaux d Alengon, represent�s par MM. Lern Ire et Bau-douin, leurs deputes.	.- .
l o � Qu'� la tenue des Etats generaux, le Tiers etat votera par t�te et non par ordre.
2� � Que les Offices de judicature soient accordes au merite et non � prix d'argent, par la raison qu'il est dangereux de confier � un jeune magistrat sans experience de decider de la fortune des citoyens, de leur Iibert6 et de leur vie ; cette t�che doit etre donnee � des hommes integres, consommes dans les lois et choisis par leurs concitoyens.
3o � Que les hautes justices soient absolument supprimees, pour eviter aux citoyens les degres de juridiction, de meme que les vicomtes, attendu la proximite des bailliages, de 5 a 10 lieues.
4� � Que les presidiaux connaissent souverainement, pour les actes civils, jusqu'� la somme 4.000 francs.
5� � Qu'il y aura un nouveau code sur l'administration de la justice, afm que les actes de procedure soient uniformes dans un siege comme dans l'autre, et saus distinction de pro-vince, avec une taxe egalement uniforme dans toute l'administration.
6� � Demander la suppression de tous privileges quel-conques.
7� � Que tous les imp�ts quelconques des villes tarifees soient supprimes pour etre remplaces dans un seul, sous teile denomination qu'on voudra lui donner, et dans les villes taillables, bourgs �t paroisses, que tous nobles, bene-fieiers et tous autres privilegies payent a proportion de leurs proprietes, comme le Tiers etat.
8� � Que la perception des imp�ts, inseree au present article, sera gratuitement faite dans les paroisses contenant 100 feux et au-dessous par deux habitants, et, pour les paroisses au-dessus, � proportion ; qu'� raison des villes tarifees ou taillables la perception se fera de la meme maniere, � raison d'un collecteur par 400 feux ; tous lesquels collec-teurs seront nommes : savoir dans les villes par la cite, et dans les paroisses par la communaute ; qu'en consequence il sera fait des r�les par MM, les officiers municipaux, qu'ils remet-tront aux dits collecteurs, duement signes et arretes dans l'ar-rondissement de chaque bailliage ; et au besoin, pour les villes seulement, les dits maire et echevins pourront commettre des employes pour la perception du droit des dites villes.
9� � Que les collecteurs, soit des villes ou des paroisses, seront tenus d'apporter, tous les trois mois, leur recette aux mains de mes dits sieurs les officiers municipaux qui seront tenus, sur le chainp et sans autres frais, de la verser au tresor royal. De l�, suppression de tous directeurs, receveurs, contr�leurs, verificateurs, inspecteurs et employes.

aux c*T�	r        general� dCS C�rV(5eS ; ct le droit
et aut'T H	T0"1"' tOUteS rcntes s�g�eu�les, foncieres
flefs	^ "^ ^'^ S�ient' et abolition dcs
11� � La suppression des banalites de moulins  fours et
utT ^����t de ''^'ite des poi!,: e't tout le royaume.
aux aux
de r^it ^ 1771 des oppositions eques, etant desastreux aux citoyens La suppression des offices de receveurs des consi-
absolument i	des
ciers mnn	,
ciers mumcipaux de chaque ville
eS gratuiteme1^ entre les mains des offi-

o !"PPress;on ^tiere des gabelles et droits d'aides. �	tarif sur les
des actes,  dans lesquels � sera libre au citoyen
r�ira -nvenable   sfns
avlnl 7 Q""d6ffses seront faites � tous seigneurs et autres ayant droit de fuie ou colombier de laisser vaquer leurs P geons depuis le ler mars jusqu,au Jer	uis g
jmllet jusqu au ler septembre, et depuis le ler octobre � >� woei par chacun an ; ainsi que d'avoir aucune garenne, dont suppression sera faite � cause de la devastatiou des grains
; le tout � peine de 1000
dommages et interets envers les habitants de la parome ou sont situes les dites fuies et colombiers.
i' Que defenses seront egalement faites � tous seigneurs, leurs gardes, domestiques, et autres particuliers, de chasser sur Jes terres ensemencees dans aucun temps, et ce sous les meines peines de 1000 livres par chaque contrevenant, applicables comme ci-dessus.
1 80 __ Que MM les officiers municipaux seront 61ug les citoyens et que leur election ne sera que de trois annees.