Fu�noten:
Pr�sidial S.3:
PRESIDIAL (1) Projet de cahier du Pr�sidial (2)
(1) Les cahiers sont publi�s dans l'ordre de comparution des d�put�s � l'Assembl�e du 28 f�vrier 1789.
(2) Le titre est de la main de Dem�es, d�put� du pr�sidial.
S.6
il ne peut �tablir aucun point de r�glement (1)
(1) Passage ratur� : � S'il n'est une d�rivation expresse de la loi�.
S. 14
tous les fonds doivent supporter les depenses necessaires a l'Etat (1).
(1) Ce cahier est �crit tout entier de la main de Dem�es, lieutenant particulier du bailliage d'Alen�on. Voir, dans notre introduction, l'influence de Dem�es sur la r�daction des cahiers de la ville, du bailliage secondaire et du bailliage principal. Se reporter �galement � l'appendice.
