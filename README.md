# KAMOKO

Welcome to your wiki! This is the default page we've installed for your convenience. Go ahead and edit it.

## Über dieses Wiki

Dieses wiki wird in [Markdown (Syntax-Hilfe)](http://daringfireball.net/projects/markdown/) geschrieben. Dies ist eine schnelle und textorientierte Auszeichnungssprache für wikis.

Das wiki selbst ist ebenfalls ein GIT-Repository. Man kann es also, klonen, lokal/online bearbeiten, commiten und pushen. Die einzelnen wiki-Seiten liegen als .md-Datei vor und können mit jedem Texteditor bearbeitet werden.

wiki clonen:

```
$ git clone https://kamoko@bitbucket.org/kamoko/kamoko.git/wiki
```

## **KA**sseler **MO**rgenstern **KO**rpus – korpusbasierte Linguistik für Studierende des Französischen

### Was ist KAMOKO?

KAMOKO ist eine strukturierte und kommentierte Sammlung von Textbeispielen zur französischen Sprache und Linguistik, die nahezu alle zentralen Strukturen der französischen Sprache aus linguistischer Sicht behandelt. Derzeit wird das digitalisierte Korpus für die Nutzung über einen Online-Zugriff vorbereitet.

Mithilfe des KAMOKO-Korpus können Studierende die Funktionen sprachlicher Formen in thematisch gegliederten Lehreinheiten für sich erschließen. Jede Einheit stellt ein sprachliches Phänomen (wie z. B. Tempus und Aspekt) anhand von Textbeispielen dar, die aufeinander aufbauen und zunehmend komplexere Verwendungen einer Form darstellen und erklären.

Zentral ist dabei das Muster von Original und Variante, bei dem der Originaltext in unterschiedlicher Weise verändert wird. Die so entstandenen Varianten und neuen Lesarten illustrieren dann das funktionale Profil einer sprachlichen Form und deren Wirken in verschiedenen Kontexten.

Auf diese Weise vermittelt KAMOKO in korpusbasierter Anschaulichkeit komplexe linguistische Inhalte.

Das KAMOKO-Korpus kann ab sofort kostenfrei genutzt werden. Besuchen Sie die folgende Seite [http://bitcut.de/app/kamoko](http://bitcut.de/app/kamoko).

Dort finden Sie den Downloadlink für eine spezielle KAMOKO-Version des CorpusExplorers (kostenloses Analyseprogramm, inkl. KAMOKO-Korpus).

Hilfe zum CorpusExplorer finden Sie unter [bitcut.de/help/ce](http://bitcut.de/help/ce).

 
KAMOKO-Équipe:

Leitung: Prof. Dr. Angela Schrott

Projektmitarbeiterinnen:

* Sandra Issel-Dombert, wissenschaftliche Mitarbeiterin
* Simone Sörensen, wissenschaftliche Mitarbeiterin
* Eve Angelique Hanschitz, studentische Hilfskraft
* Jan-Oliver Rüdiger, IT-Consulting
* Tanja Krasny, Teamassistenz

Ex-Projektmitarbeiterinnen:

* Janine Gentzsch, studentische Hilfskraft
* Stefanie Stroh, studentische Hilfskraft
* Elisa Nolte, studentische Hilfskraft

KAMOKO wird als E-Learning Projekt durch die Universität Kassel gefördert. 

### KAMOKO - FAQ

**Sind die Beispiele (Original/Variante) real oder konstruiert?**
Die Beispiele sind aus französischsprachigen Romanen (größtenteils) und z. T. aus französischer Sachliteratur entnommen. Damit sind die Beispiele authentisch. Die Varianten sind konstruiert, z. B. wird die eine Verbform die im Original im passé simple steht in andere Vergangenheitstempora variiert; dahinter steckt dann z. B. die Frage, wie sich die Semantik des Satzes durch die Wahl des Tempus verändert/nuanciert wird.

**Wie sollen Studenten mit den Textbeispielen arbeiten?**
Die Studenten sollten auf die Funktion/Wirkung schließen und die Form abfragen können.

**OBSOLET ab Oktober 2016 (siehe unten): In den XML-Dateien taucht *!EMPTY!* auf, was bedeutet das?**

!EMPTY! ist ein Platzhalter für […]-Auslassungsklammern. Der Konverter ersetzt in den Digitalisaten alle […] durch !EMPTY!. Im letzten noch anstehenden Konverter-Schritt wird das !EMPTY! dann wieder durch […] zurück übersetzt.

*Warum ist das nötig?:* Der TreeTagger den wir für die Annotation von POS/Lemma Daten verwenden erkennt die Auslassungsklammer […] nicht korrekt. Er würde Sie als zwei Klammern die drei leere Sätze enthalten identifizieren.

*Wie verfahre ich mit !EMPTY! ?:*

* Falls !EMPTY! eine Lücke im Sinne einer Auslassung im Text darstellt (z. B. um die Auslassung im zitiertem Text darzustellen), dann wird das !EMPTY! so belassen wie es ist.

* Falls !EMPTY! eine Lücke ist, die von den Muttersprachlern als Lücke im Sinne es Lückentextes verwendet wird (zu erkennen: Eintragungen mit Bleistift), wird !EMPTY! als Variante mit allen Lückenfüllern annotiert.

* Falls !EMPTY! in einem anderen Kontext auftreten sollte, bitte nachfragen.

**NEU: Lösung für !EMPTY! und [...]**

Da ab Oktober 2016 alle Kurse im Format *.kamoko.xml vorliegen, kann entweder [...] oder !EMPTY! als Platzhalter verwendet werden. Vorzugsweise (zur besseren Lesbarkeit) sollte [...] genutzt werden.

**Wie ist das GIT-Repository strukturiert? - Wo finde ich welche Dateien?**
Die aktuelle Struktur orientiert sich an den einzelnen Bearbeitungsschritten des XML-Konvertierungsprozesses:

* *"0 - Digitalisate"* - Hier sind die digitalisierten Blätter als *.doc-Textdateien gespeichert. Hier bitte *KEINE ÄNDERUNGEN* mehr vornehmen. Der Ordner gilt als: **abgeschlossen**.

* *"0B - Cahiers de doléances"* - Dieser Ordner enthält zusätzliche *.txt-Textdateien mit Auszügen. Hier bitte *KEINE ÄNDERUNGEN* mehr vornehmen. Der Ordner gilt als: **abgeschlossen**.

* *"1 - Konvertierte Word-HTML-Dateien"* - Dieser Ordner enthält das Resultat der Konvertierung WORD > HTML via "Microsoft Word 2013" (Speichern unter... > Als Webseite (*.htm)). Der Inhalt des Ordners entspricht weitgehend dem des Ordners "0 - Digitalisate". Nur das hier die Dokumente als HTML vorliegen. Hier bitte *KEINE ÄNDERUNGEN* mehr vornehmen. Der Ordner gilt als: **abgeschlossen**.

* *"1B - Konvertierte Excel-Tabellen"* - In einer frühen Projektphase gab es den Versuch die Daten als Excel-Sheet zu annotieren. Dieser Versuch wurde aufgrund der geringen Annotationsgeschwindigkeit und dem hohen Aufwand eingestellt. Für diese Dateien exsistiert ein gesonderter Konverter. Hier bitte *KEINE ÄNDERUNGEN* mehr vornehmen. Der Ordner gilt als: **abgeschlossen**.

* *"2 - XML-Dateien"* - Dieser Ordner enthält alle in XML-Konvertierten Dokumente. Diese müssen Überprüft werden, bevor eine Weiterverarbeitung stattfinden kann. [Eine Anleitung zur Korrektur finden Sie hier.](XML-Dateien korrigieren) - Neben den XML-Dateien befinden sich auch die Fehlerberichte des Konverters in diesem Ordner. Fertig überprüfte Dateien werden in den Ornder "3 - XML-Dateien überprüft" verschoben. *Wichtig:* Letzte Änderung speichern > commiten > verschieben > commiten > pushen - So bleibt die Dateihistorie auf allen Systemen sichtbar erhalten.  Hier bitte *KEINE ÄNDERUNGEN* mehr vornehmen. Der Ordner gilt als: **abgeschlossen**.

* *"3 - XML-Dateien überprüft"* - Dieser Ordner enthält die in korrigierten XML-Dateien und die dazu gehörigen .speaker.xml-Dateien (Metadaten-Erweiterungen: Sprecherannotationen) die gerade bearbeitet werden.  Fertig überprüfte Dateien werden in den Ornder "4 - XML-Dateien überprüft & angereichert" verschoben. *Wichtig:* Letzte Änderung speichern > commiten > verschieben > commiten > pushen - So bleibt die Dateihistorie auf allen Systemen sichtbar erhalten. Hier bitte *KEINE ÄNDERUNGEN* mehr vornehmen. Der Ordner gilt als: **abgeschlossen**.

* *"4 - XML-Dateien überprüft & angereichert"* - Enthält alle fertigen XML-Dateien sowie die entsprechenden Metadaten-Erweiterungen (z.B.: .speaker.xml). Weitere Metadaten-Erweiterungen können hinzugefügt werden z. B. linguistische Kommentare. Die Original .xml uns .speaker.xml sollten in diesem Prozesschritt *nicht mehr verändert* werden. Hier bitte *KEINE ÄNDERUNGEN* mehr vornehmen. Der Ordner gilt als: **abgeschlossen** - Alle Kurse wurden in das .kamoko.xml-Format konvertiert. Um die Suche in den alten Kursen zu vereinfachen, wurde eine Backup-Datei hinterlegt - siehe: 4 - XML-Dateien.zip

* *"5 - KAMOKO-XML"* - Das KAMOKO-XML-Format wurde in der zweiten Projektphase entwickelt. Für dieses Format steht ein eigens entwickelter [Editor<http://bitcut.de/products/CorpusExplorer.Tool4.KAMOKO/publish.htm] kostenfrei zur Verfügung. Ziel der zweiten Projektphase war die Beschleunigung des kompletten Digitalisierungs-/Annotationsprozesses.

* *"6 - CorpusExplorer v1.0 Korpora"* - Fertig konvertierte CorpusExplorer-Korpora auf Basis der in "4 - XML-Dateien überprüft & angereichert" hinterlegten XML-Daten. Hier bitte *KEINE ÄNDERUNGEN* mehr vornehmen. Der Ordner gilt als: **abgeschlossen**.

* *"7 - CorpusExplorer v2.0 Korpora"* - Fertig konvertierte CorpusExplorer-Korpora auf Basis der in "4 - XML-Dateien überprüft & angereichert" hinterlegten XML-Daten sowie auf Basis der Daten aus *"5 - KAMOKO-XML"*.

* *"8 - Konfigurationen"* - Diese Ordner beihnaltet Konfigurationen und Fixes. z. B. empty.fix - dieses Datei wurde für die Erstellung des commits [82f34ff13b789d6734676ec2d6ed651d26ce7ec2|https://bitbucket.org/kamoko/kamoko/commits/82f34ff13b789d6734676ec2d6ed651d26ce7ec2?at=master] verwendet. Damit werden alle fehlerhaften !EMPTY!-Auszeichnungen korrigiert.

* *"Anmerkungen"* - Ordner für Anmerkungen

* *"Schema"* - Ordner, der die XML-Schema-Definitionen enthält.

* *"Vorträge"* - Ordner, der Vortragsmaterial zu KAMOKO enthält.